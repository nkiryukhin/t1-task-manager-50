package ru.t1.nkiryukhin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    List<User> findAll();

    int getSize();

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findOneById(@NotNull String id);

}