package ru.t1.nkiryukhin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.model.ISessionRepository;
import ru.t1.nkiryukhin.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Session m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

}
