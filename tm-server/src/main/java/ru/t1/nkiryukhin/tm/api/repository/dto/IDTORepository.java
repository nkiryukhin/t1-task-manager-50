package ru.t1.nkiryukhin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.model.AbstractModelDTO;

public interface IDTORepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

}
