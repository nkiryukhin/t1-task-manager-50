package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.component.ISaltProvider;

public interface IPropertyService extends IDatabaseProperty, ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDbaPassphrase();

    @NotNull
    String getBrokerAddress();

}