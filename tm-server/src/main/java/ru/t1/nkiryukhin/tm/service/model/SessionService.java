package ru.t1.nkiryukhin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.model.ISessionRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.model.ISessionService;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.model.Session;
import ru.t1.nkiryukhin.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionService extends AbstractService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractFieldException {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(final List<Session> sessions) {
        for (final Session session : sessions) {
            removeOne(session);
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Session session = findOneById(id);
        removeOne(session);
    }

    @Override
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        List<Session> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        remove(sessions);
    }

}
